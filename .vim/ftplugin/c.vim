
" {{{ Automatic close char mapping

" More common in PEAR coding standard
inoremap  { {<CR>}<C-O>O
" Maybe this way in other coding standards
" inoremap  { <CR>{<CR>}<C-O>O

inoremap [ []<LEFT>

" Standard mapping after PEAR coding standard
inoremap ( ()<LEFT> 

" Maybe this way in other coding standards
" inoremap ( ( )<LEFT><LEFT> 

inoremap " ""<LEFT>
inoremap ' ''<LEFT>

" }}} Automatic close char mapping
