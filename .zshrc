# .zshrc

### 基本設定
HISTFILE=$HOME/.zsh_history           # 履歴をファイルに保存する
HISTSIZE=1000000                      # メモリ内の履歴の数
SAVEHIST=1000000                      # 保存される履歴の数
function history-all { history -E 1 } # 全履歴の一覧を出力する

### ssh-agentの設定
#agent="$HOME/tmp/ssh-agent-$USER"
#if [ -S "$SSH_AUTH_SOCK" ]; then
#    case $SSH_AUTH_SOCK in
#    /tmp/*/agent.[0-9]*)
#        ln -snf "$SSH_AUTH_SOCK" $agent && export SSH_AUTH_SOCK=$agent
#    esac
#elif [ -S $agent ]; then
#    export SSH_AUTH_SOCK=$agent
#else
#    echo "no ssh-agent"
#fi

### 環境変数
export LANG="ja_JP.utf-8"
export PAGER="/usr/bin/less"
export TMP="$HOME/tmp"
export EDITOR="/usr/local/bin/vim"
export GREP_OPTIONS="--color=auto"
export USER="kiske"
export HOSTNAME="serverman"
#export PATH=/opt/local/bin:/opt/local/sbin:$PATH
#export MANPATH=/opt/local/man:$MANPATH

### 非端末プロセスなら終了
#[ $#PROMPT -eq 0 -o $#TERM -eq 0 ] && return

# PROMPT, RPROMPT
local GREEN=$'%{\e[1;32m%}'
local BLUE=$'%{\e[1;34m%}'
local DEFAULT=$'%{\e[m%}'

export PROMPT=$BULE'[${USER}@${HOSTNAME}] %(!.#.$) '$DEFAULT
export RPROMPT=$GREEN'[%~]'$DEFAULT

# ファイル補完候補に色づけ
#eval `dircolors -b`
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}

# 補完関数の表示を過剰に
#zstyle ':completion:*' verbose yes
#zstyle ':completion:*:descriptions' format '%B%d%b'
#zstyle ':completion:*:messages' format '%d'
#zstyle ':completion:*:warnings' format 'No matches for: %d'
#zstyle ':completion:*' group-name ''

# 補完候補をカーソル選択
zstyle ':completion:*:default' menu select true

# 補完候補の大文字小文字の違いを無視
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'

### エイリアス設定
# User specific aliases and functions
#alias vi='/opt/local/bin/vim'
alias vi='/usr/local/bin/vim'
#alias cdap='cd /opt/local/apache2/htdocs'
alias ls='/bin/ls -F --color'
alias la='/bin/ls -a --color'
alias ll='/bin/ls -lg --color'
alias le='less -e'
alias pd=pushd
alias du="du -h"
alias df="df -h"

alias -g L="| $PAGER"
alias -g G="| grep"
alias -g T="| tee -a logfile"
alias -g W="| wc"
alias -g H="| head"
alias -g TL="| tail"

alias h="history 20"
alias more='less'

### Autocomplete
autoload -U compinit
compinit -u

### シェル変数設定

# 複数の zsh を同時に使う時など history ファイルに上書きせず追加する
setopt append_history

# 指定したコマンド名がなく、ディレクトリ名と一致した場合 cd する
setopt auto_cd

# 補完候補が複数ある時に、一覧表示する
setopt auto_list

# 補完キー（Tab, Ctrl+I) を連打するだけで順に補完候補を自動で補完する
setopt auto_menu

# カッコの対応などを自動的に補完する
setopt auto_param_keys

# ディレクトリ名の補完で末尾の / を自動的に付加し、次の補完に備える
setopt auto_param_slash

# 最後がディレクトリ名で終わっている場合末尾の / を自動的に取り除く
#setopt auto_remove_slash

# サスペンド中のプロセスと同じコマンド名を実行した場合はリジュームする
setopt auto_resume

# ビープ音を鳴らさないようにする
setopt no_beep

# {a-c} を a b c に展開する機能を使えるようにする
setopt brace_ccl

# 内部コマンドの echo を BSD 互換にする
#setopt bsd_echo

# シンボリックリンクは実体を追うようになる
#setopt chase_links

# 既存のファイルを上書きしないようにする
#setopt clobber

# コマンドのスペルチェックをする
setopt correct

# コマンドライン全てのスペルチェックをする
#setopt correct_all

# =command を command のパス名に展開する
setopt equals

# ファイル名で #, ~, ^ の 3 文字を正規表現として扱う
setopt extended_glob

# zsh の開始・終了時刻をヒストリファイルに書き込む
#setopt extended_history

# Ctrl+S/Ctrl+Q によるフロー制御を使わないようにする
setopt NO_flow_control

# 各コマンドが実行されるときにパスをハッシュに入れる
#setopt hash_cmds

# コマンドラインの余計なスペースを排除
setopt hist_reduce_blanks

# 直前と同じコマンドラインはヒストリに追加しない
setopt hist_ignore_dups

# コマンドラインの先頭がスペースで始まる場合ヒストリに追加しない
setopt hist_ignore_space

# ヒストリを呼び出してから実行する間に一旦編集できる状態になる
setopt hist_verify

# シェルが終了しても裏ジョブに HUP シグナルを送らないようにする
setopt NO_hup

# Ctrl+D では終了しないようになる（exit, logout などを使う）
setopt ignore_eof

# コマンドラインでも # 以降をコメントと見なす
setopt interactive_comments

# auto_list の補完候補一覧で、ls -F のようにファイルの種別をマーク表示
setopt list_types

# 内部コマンド jobs の出力をデフォルトで jobs -l にする
setopt long_list_jobs

# コマンドラインの引数で --prefix=/usr などの = 以降でも補完できる
setopt magic_equal_subst

# メールスプール $MAIL が読まれていたらワーニングを表示する
#setopt mail_warning

# ファイル名の展開でディレクトリにマッチした場合末尾に / を付加する
setopt mark_dirs

# 補完候補が複数ある時、一覧表示 (auto_list) せず、すぐに最初の候補を補完する
#setopt menu_complete

# 複数のリダイレクトやパイプなど、必要に応じて tee や cat の機能が使われる
setopt multios

# ファイル名の展開で、辞書順ではなく数値的にソートされるようになる
setopt numeric_glob_sort

# コマンド名に / が含まれているとき PATH 中のサブディレクトリを探す
#setopt path_dirs

# 8 ビット目を通すようになり、日本語のファイル名などを見れるようになる
setopt print_eightbit

# 戻り値が 0 以外の場合終了コードを表示する
#setopt print_exit_value

# ディレクトリスタックに同じディレクトリを追加しないようになる
setopt pushd_ignore_dups

# pushd を引数なしで実行した場合 pushd $HOME と見なされる
#setopt pushd_to_home

# rm * などの際、本当に全てのファイルを消して良いかの確認しないようになる
#setopt rm_star_silent

# rm_star_silent の逆で、10 秒間反応しなくなり、頭を冷ます時間が与えられる
#setopt rm_star_wait

# for, repeat, select, if, function などで簡略文法が使えるようになる
setopt short_loops

# デフォルトの複数行コマンドライン編集ではなく、１行編集モードになる
#setopt single_line_zle

# コマンドラインがどのように展開され実行されたかを表示するようになる
#setopt xtrace

# 色を使う
setopt prompt_subst

# シェルのプロセスごとに履歴を共有
setopt share_history

# history (fc -l) コマンドをヒストリリストから取り除く。
setopt hist_no_store

# 文字列末尾に改行コードが無い場合でも表示する
unsetopt promptcr

#コピペの時rpromptを非表示する
setopt transient_rprompt

# cd -[tab]でpushd
setopt autopushd


### キーバインド設定
bindkey -e

precmd() {echo -ne "\033]0;${HOST} : $PWD\007"}

function chpwd() {
  ls
}
#function cdup() {
#  cd ..
#  zle reset-prompt
#}
#zle -N cdup
#bindkey '\^' cdup

### screen window name
preexec () {
    [ ${STY} ] && echo -ne "\ek${1%% *}\e\\"  
}

#if [ "$TERM" = "screen" ]; then
#  preexec() {
#    # see [zsh-workers:13180]
#    # http://www.zsh.org/mla/workers/2000/msg03993.html
#    emulate -L zsh
#    local -a cmd; cmd=(${(z)2})
#    echo -n "^[k$cmd[1]:t^[\\"
#  }
#fi
