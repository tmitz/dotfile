" An example for a vimrc file.
"
" Maintainer:	Bram Moolenaar <Bram@vim.org>
" Last change:	2008 Jul 02
"
" To use it, copy it to
"     for Unix and OS/2:  ~/.vimrc
"	      for Amiga:  s:.vimrc
"  for MS-DOS and Win32:  $VIM\_vimrc
"	    for OpenVMS:  sys$login:.vimrc

" When started as "evim", evim.vim will already have done these settings.
if v:progname =~? "evim"
  finish
endif

" PrivatePorts vim settings
set iminsert=0 imsearch=0
" for Vim.app
"set antialias

" Use Vim settings, rather then Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible

" allow backspacing over everything in insert mode
" set backspace=indent,eol,start

if has("vms")
  set nobackup		" do not keep a backup file, use versions instead
else
  set backup		" keep a backup file
endif
set history=50		" keep 50 lines of command line history

" For Win32 GUI: remove 't' flag from 'guioptions': no tearoff menu entries
" let &guioptions = substitute(&guioptions, "t", "", "g")

" Don't use Ex mode, use Q for formatting
map Q gq

" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
inoremap <C-U> <C-G>u<C-U>

" In many terminal emulators the mouse works just fine, thus enable it.
if has('mouse')
  set mouse=a
endif

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
  syntax on
  set hlsearch
endif

" Only do this part when compiled with support for autocommands.
if has("autocmd")

  " Enable file type detection.
  " Use the default filetype settings, so that mail gets 'tw' set to 72,
  " 'cindent' is on in C files, etc.
  " Also load indent files, to automatically do language-dependent indenting.
  filetype plugin indent on

  " Put these in an autocmd group, so that we can delete them easily.
  augroup vimrcEx
  au!

  " For all text files set 'textwidth' to 78 characters.
  autocmd FileType text setlocal textwidth=78

  " When editing a file, always jump to the last known cursor position.
  " Don't do it when the position is invalid or when inside an event handler
  " (happens when dropping a file on gvim).
  " Also don't do it when the mark is in the first line, that is the default
  " position when opening a file.

  " 前回終了したカーソル行に移動
  autocmd BufReadPost *
    \ if line("'\"") > 1 && line("'\"") <= line("$") |
    \   exe "normal! g`\"" |
    \ endif

  augroup END

else

  set autoindent		" always set autoindenting on

endif " has("autocmd")

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r # | 0d_ | diffthis
		  \ | wincmd p | diffthis
endif

" ChangeLog
let g:changelog_dateformat = "%Y-%m-%d"
let g:changelog_username = "<Tomohiro Mitsumune>"

" Reads the skeleton php file
" Note: The normal command afterwards deletes an ugly pending line and moves
" the cursor to the middle of the file.
autocmd BufNewFile *.php 0r ~/.vim/skeleton.php | normal Gdd

" Use filetype plugins, e.g. for PHP
" syntax on
" filetype plugin on

" setting omnifunc for filetype
if has("autocmd") && exists("+omnifunc")
    autocmd FileType php        :set omnifunc=phpcomplete#CompletePHP 
    autocmd FileType php        :set tags+=$HOME/.vim/symfony.tags
    autocmd FileType php        :set dictionary+=$HOME/.vim/php-functions.dict
    autocmd FileType html       :set omnifunc=htmlcomplete#CompleteTags
    autocmd FileType python     :set omnifunc=pythoncomplete#Complete
    autocmd FileType javascript :set omnifunc=javascriptcomplete#CompleteJS
    autocmd FileType xml        :set omnifunc=xmlcomplete#CompleteTags
    autocmd FileType css        :set omnifunc=csscomplete#CompleteCSS
    autocmd FileType c          :set omnifunc=ccomplete#Complete
    autocmd Filetype * 
                \   if &omnifunc == "" |
                \           setlocal omnifunc=syntaxcomplete#Complete |
                \   endif
endif

" setting minibufexpl
let g:miniBufExplMapWindowNavVim = 1
let g:miniBufExplSplitBelow = 0  " Put new window above
let g:miniBufExplMapWindowNavArrows = 1
let g:miniBufExplMapCTabSwitchBufs = 1
let g:miniBufExplModSelTarget = 1
let g:miniBufExplSplitToEdge = 1

" Show nice info in ruler
set ruler
set laststatus=2

" Set backup setting
set nobackup
 
" Set standard setting for PEAR coding standards
set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4

" Show line numbers by default
set nonumber

" Enable folding by fold markers
set foldmethod=marker 

" Autoclose folds, when moving out of them
set foldclose=all

" don`t Use incremental searching
"検索文字列入力時に順次対象文字列にヒットさせない
set noincsearch

" Do not highlight search results
"検索結果文字列のハイライトを有効にしない
set nohlsearch

" Jump 5 lines when running out of the screen
set scrolljump=5

" Indicate jump out of the screen when 3 lines before end of the screen
set scrolloff=3

" Repair wired terminal/vim settings
set backspace=start,eol,indent

" Allow file inline modelines to provide settings
set modeline

"スマートインデント
set smartindent

"検索文字列に大文字が含まれている場合は区別して検索する
set smartcase

"検索時に最後まで行ったら最初に戻る
set wrapscan

"タブの左側にカーソル表示
"set listchars=tab:\\ 
set nolist

"入力中のコマンドをステータスに表示
set showcmd

"括弧入力時の対応する括弧を表示
set showmatch

function! GetB()"{{{
 let c = matchstr(getline('.'), '.', col('.') - 1)
  let c = iconv(c, &enc, &fenc)
  return String2Hex(c)
endfunction
" :help eval-examples
" The function Nr2Hex() returns the Hex string of a number.
func! Nr2Hex(nr)
  let n = a:nr
  let r = ""
  while n
    let r = '0123456789ABCDEF'[n % 16] . r
    let n = n / 16
  endwhile
  return r
endfunc
" The function String2Hex() converts each character in a string to a two
" character Hex string.
func! String2Hex(str)
  let out = ''
  let ix = 0
  while ix < strlen(a:str)
    let out = out . Nr2Hex(char2nr(a:str[ix]))
    let ix = ix + 1
  endwhile
  return out
endfunc"}}}

"ステータスラインに文字コードと改行文字を表示する"{{{
if winwidth(0) >= 120
  set statusline=%<[%n]%m%r%h%w%{'['.(&fenc!=''?&fenc:&enc).':'.&ff.']'}%y\ %F%=[%{GetB()}]\ %l,%c%V%8P
else
  set statusline=%<[%n]%m%r%h%w%{'['.(&fenc!=''?&fenc:&enc).':'.&ff.']'}%y\ %f%=[%{GetB()}]\ %l,%c%V%8P
endif"}}}

" コマンドライン補完するときに強化されたものを使う(参照 :help wildmenu)
" set wildmenu
" コマンドライン補間をシェルっぽく
set wildmode=list:longest
" バッファが編集中でもその他のファイルを開けるように
set hidden
" 外部のエディタで編集中のファイルが変更されたら自動で読み直す
set autoread

" 文字コードの自動認識"{{{
if &encoding !=# 'utf-8'
  set encoding=japan
  set fileencoding=japan
endif
if has('iconv')
  let s:enc_euc = 'euc-jp'
  let s:enc_jis = 'iso-2022-jp'
  " iconvがeucJP-msに対応しているかをチェック
  if iconv("\x87\x64\x87\x6a", 'cp932', 'eucjp-ms') ==# "\xad\xc5\xad\xcb"
    let s:enc_euc = 'eucjp-ms'
    let s:enc_jis = 'iso-2022-jp-3'
  " iconvがJISX0213に対応しているかをチェック
  elseif iconv("\x87\x64\x87\x6a", 'cp932', 'euc-jisx0213') ==# "\xad\xc5\xad\xcb"
    let s:enc_euc = 'euc-jisx0213'
    let s:enc_jis = 'iso-2022-jp-3'
  endif
  " fileencodingsを構築
  if &encoding ==# 'utf-8'
    let s:fileencodings_default = &fileencodings
    if has('unix')
        let &fileencodings = s:enc_jis .','. s:enc_euc
        let &fileencodings = &fileencodings .','. s:fileencodings_default
    else
        let &fileencodings = s:enc_jis .','. s:enc_euc .',cp932'
        let &fileencodings = &fileencodings .','. s:fileencodings_default
    endif
    unlet s:fileencodings_default
  else
    let &fileencodings = &fileencodings .','. s:enc_jis
    set fileencodings+=utf-8,ucs-2le,ucs-2
    if &encoding =~# '^\(euc-jp\|euc-jisx0213\|eucjp-ms\)$'
      set fileencodings+=cp932
      set fileencodings-=euc-jp
      set fileencodings-=euc-jisx0213
      set fileencodings-=eucjp-ms
      let &encoding = s:enc_euc
      let &fileencoding = s:enc_euc
    else
      let &fileencodings = &fileencodings .','. s:enc_euc
    endif
  endif
  " 定数を処分
  unlet s:enc_euc
  unlet s:enc_jis
endif

" 日本語を含まない場合は fileencoding に encoding を使うようにする
if has('autocmd')
  function! AU_ReCheck_FENC()
    if &fileencoding =~# 'iso-2022-jp' && search("[^\x01-\x7e]", 'n') == 0
      let &fileencoding=&encoding
    endif
  endfunction
  autocmd BufReadPost * call AU_ReCheck_FENC()
endif
" 改行コードの自動認識
set fileformats=unix,dos,mac
" □とか○の文字があってもカーソル位置がずれないようにする
if exists('&ambiwidth')
  set ambiwidth=double
endif"}}}

" cvs,svnの時は文字コードをutf-8に設定
autocmd FileType cvs :set fileencoding=utf-8
autocmd FileType svn :set fileencoding=utf-8

" command mode 時 tcsh風のキーバインドに
cmap <C-A> <Home>
cmap <C-F> <Right>
cmap <C-B> <Left>
cmap <C-D> <Delete>
cmap <Esc>b <S-Left>
cmap <Esc>f <S-Right>

"表示行単位で行移動する
nmap j gj
nmap k gk
vmap j gj
vmap k gk

",e でそのコマンドを実行
nmap ,e :execute '!' &ft ' %'<CR>

" 補完候補の色づけ for vim7
hi Pmenu ctermbg=8
hi PmenuSel ctermbg=12
hi PmenuSbar ctermbg=10

" 256色
" set t_Co=256
" set t_Sf=[3%dm
" set t_Sb=[4%dm

" encoding
nmap ,U :set encoding=utf-8<CR>
nmap ,E :set encoding=euc-jp<CR>
nmap ,S :set encoding=cp932<CR>

" closetag
"autocmd FileType html,xml,xsl,php  :source ~/.vim/scripts/closetag.vim

"Omni補完のキー割り当て変更
inoremap <C-F> <C-X><C-O>

" paste/nopaste
nmap ep :set paste<CR>
nmap enp :set nopaste<CR>

" fuzzyfinder
let g:FuzzyFinderOptions = { 'Base':{}, 'Buffer':{}, 'File':{}, 'Dir':{}, 'MruFile':{}, 'MruCmd':{}, 'Bookmark':{}, 'Tag':{}, 'TaggedFile':{}}
let g:FuzzyFinderOptions.Base.ignore_case = 1
let g:FuzzyFinderOptions.MruFile.max_item = 200
let g:FuzzyFinderOptions.MruCmd.max_item = 200

nmap <silent> eff :FuzzyFinderFile<CR>
nmap <silent> efv :FuzzyFinderFavFile<CR>
nmap <silent> efm :FuzzyFinderMruFile<CR>
nmap <silent> efc :FuzzyFinderMruCmd<CR>
nmap <silent> efd :FuzzyFinderDir<CR>
nmap <silent> efa :FuzzyFinderAddFavFile<CR>
nmap <silent> eft :FuzzyFinderTag<CR>
nmap <silent> efb :FuzzyFinderBuffer<CR>

" surround.vim
let g:surround_{char2nr('e')} = "begin \r end" 

" snippetsEmu.vim
let g:snippetsEmu_key = "<C-b>"

" copy & paste mappings
nmap _ :.w !nkf -Ws\|pbcopy<CR><CR>
vmap _ :w !nkf -Ws\|pbcopy<CR><CR>
nmap - :set paste<CR>:r !pbpaste\|nkf -Sw<CR>:set nopaste<CR>

" set tags
if has("autochdir")
  set autochdir
  set tags+=tags
else
  set tags+=./tags,./../tags,./*/tags,./../../tags,./../../../tags,./../../../../tags
endif
nnoremap <silent> ,T :TlistToggle<CR>

" yankring.vim
nnoremap <silent> ,y :YRShow<CR>
nnoremap dd :<C-U>YRYankCount 'dd'<CR>
nnoremap D :<C-U>YRYankCount 'D'<CR>
nnoremap Y :<C-U>YRYankCount 'Y'<CR>

" twitvim.vim
let twitvim_login = "xxxx:xxxx"
nnoremap <silent> ,t :PosttoTwitter<CR>

